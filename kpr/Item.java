/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package kpr;



public class Item {
	private int m_weight = 0;
	private int m_value = 0;
	private int m_bounding = 1;
	private int m_inKnapsack = 0;
	private int m_id;
	private int m_nodeId;

	/*
	public Item(int number) {
		m_number = number;
	}

	public Item(Item item) {
		setWeight(item.m_weight);
		setValue(item.m_value);
		setBounding(item.m_bounding);
	}
*/
	public Item(int number, int nodeId, int weight, int value) {
		m_id = number;
		m_nodeId = nodeId;
		setWeight(weight);
		setValue(value);
	}
/*
	public Item(int number, int weight, int value, int bounding) {
		m_number = number;
		setWeight(weight);
		setValue(value);
		setBounding(bounding);
	}
*/
	public void setWeight(int weight) {
		m_weight = Math.max(weight, 0);
	}

	public void setValue(int value) {
		m_value = Math.max(value, 0);
	}

	public void setInKnapsack(int inKnapsack) {
		m_inKnapsack = Math.min(getBounding(), Math.max(inKnapsack, 0));
	}

	public void setBounding(int bounding) {
		m_bounding = Math.max(bounding, 0);
		if (m_bounding == 0)
			m_inKnapsack = 0;
	}

	public void checkMembers() {
		setWeight(m_weight);
		setValue(m_value);
		setBounding(m_bounding);
		setInKnapsack(m_inKnapsack);
	}

	public int getWeight() {
		return m_weight;
	}

	public int getValue() {
		return m_value;
	}

	public int getInKnapsack() {
		return m_inKnapsack;
	}

	public int getBounding() {
		return m_bounding;
	}
	public Integer getId() {
		return m_id;
	}
	
	public Integer getNodeId() {
		return m_nodeId;
	}
}
