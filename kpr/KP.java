/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package kpr;

import java.util.ArrayList;
import java.util.List;

public class KP {

	private List<Item> m_itemList = new ArrayList<Item>();
	private int m_maxWeight = 0;
	private int m_solutionWeight = 0;
	private int m_value = 0;
	private boolean calculated = false;

	public KP(int kpSize)
	{
		m_maxWeight = kpSize;
	}
	
	public int solve(List<Item> solution) {
		int n = m_itemList.size();

		initialize();
		if (n > 0 && m_maxWeight > 0) {
			List<List<Integer>> c = new ArrayList<List<Integer>>();
			List<Integer> curr = new ArrayList<Integer>();

			c.add(curr);
			for (int j = 0; j <= m_maxWeight; j++)
				curr.add(0);
			for (int i = 1; i <= n; i++) {
				List<Integer> prev = curr;
				c.add(curr = new ArrayList<Integer>());
				for (int j = 0; j <= m_maxWeight; j++) {
					if (j > 0) {
						int wH = m_itemList.get(i - 1).getWeight();
						curr.add((wH > j) ? prev.get(j) : Math.max(
								prev.get(j),
								m_itemList.get(i - 1).getValue()
										+ prev.get(j - wH)));
					} else {
						curr.add(0);
					}
				}
			}
			m_value = curr.get(m_maxWeight);

			for (int i = n, j = m_maxWeight; i > 0 && j >= 0; i--) {
				int tempI = c.get(i).get(j);
				int tempI_1 = c.get(i - 1).get(j);
				if ((i == 0 && tempI > 0) || (i > 0 && tempI != tempI_1)) {
					Item iH = m_itemList.get(i - 1);
					int wH = iH.getWeight();
					iH.setInKnapsack(1);
					j -= wH;
					m_solutionWeight += wH;
				}
			} // for()
			calculated = true;
		} // if()
		solution.clear();
		for(Item item : m_itemList)
		{
			if(item.getInKnapsack() == 1) solution.add(item);
		}
		return m_value;
	}

	
	
	public void add(Item item) {
		m_itemList.add(item);
		initialize();
	}



	public int getSolutionWeight() {
		return m_solutionWeight;
	}

	public boolean isCalculated() {
		return calculated;
	}

	public int getMaxWeight() {
		return m_maxWeight;
	}

	public void setMaxWeight(int maxWeight) {
		m_maxWeight = Math.max(maxWeight, 0);
	}

	public void setItemList(List<Item> itemList) {
		if (itemList != null) {
			m_itemList = itemList;
			for (Item item : itemList) {
				item.checkMembers();
			}
		}
	}


	private void setInKnapsackByAll(int inKnapsack) {
		for (Item item : m_itemList)
			if (inKnapsack > 0)
				item.setInKnapsack(1);
			else
				item.setInKnapsack(0);
	}

	
	protected void initialize() {
		setInKnapsackByAll(0);
		calculated = false;
		m_value = 0;
		m_solutionWeight = 0;
	}

}
