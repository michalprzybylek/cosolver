/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package tskp;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;




public class TSKP {
	private Node m_end;
	private int m_kpSize;
	private double m_vMin;
	private double m_vMax;
	private int m_size;
	
	

	
	public TSKP(Node end, int kpSize, double vMin, double vMax)
	{
		m_end = end;
		m_kpSize = kpSize;
		m_vMin = vMin;
		m_vMax = vMax;
		
		//initialise();
	}

	public void printGraph(Node node)
	{
		if(node.isVisited()) return;
		node.visit();
		System.out.print(node.getId() + " --->");
		for(WeightedTarget nexts : node.getNexts())
		{
			System.out.print(", " + nexts.target().getId());
		}
		System.out.println("");
		
		for(WeightedTarget nexts : node.getNexts())
		{
			printGraph(nexts.target());
		}
		
	}
	
	private void initialise() {
		Set<Node> acc = new HashSet<Node>();
		unvisit(m_end, acc);
		m_size = acc.size();
	}
	
	private void unvisit(Node node, Set<Node> acc)
	{
		if(acc.contains(node)) return;
		node.unvisit();
		acc.add(node);
		for(WeightedTarget next : node.getNexts())
		{
			unvisit(next.target(), acc);
		}
	}

	public double solve(List<Node> solution)
	{
		initialise();
		
		//System.out.println("++++++++++");
		//printGraph(m_end);
		//System.out.println("++++++++++");

		//initialise();
		
		double cost = solver(m_end, solution, 0, 0, Double.MAX_VALUE, 0);
		Collections.reverse(solution);
		return cost;
	}
	
	private int countFreeNodes(Node start)
	{
		if(start.isTouched()) return 0;
		start.mark();
		int count = 1;
		for(WeightedTarget arrow : start.getNexts())
		{
			Node nextNode = arrow.target();
			count += countFreeNodes(nextNode);
		}
		return count;
	}
	
	private int unmarkFreeNodes(Node start)
	{
		if(!start.isMarked()) return 0;
		start.unmark();
		int count = 1;
		for(WeightedTarget arrow : start.getNexts())
		{
			Node nextNode = arrow.target();
			count += unmarkFreeNodes(nextNode);
		}
		return count;
	}
	
	private boolean checkConsistency(Node start, int k)
	{
		int counted = countFreeNodes(start);
		int unmarked = unmarkFreeNodes(start);
	
		if(counted != unmarked)
		{
			System.out.println("Counted " + counted + " unmarked " + unmarked + " k+counted " + (k + counted) + " size " + m_size);
			System.exit(0);
		}
		
		return counted + k == m_size;
	}
	
	
	private double solver(Node start, List<Node> bestRoute, int totalWeight, double totalCost, double m, int k)
	{
		//System.out.println("***" + start.getId());
		double min = m;
		if(start == m_end)
		{
			
			if(k == m_size) // found a Hamiltonian cycle
			{
				if(totalCost < min) // better solution
				{
					//System.out.println("Stack starts with total cost " + totalCost);
					bestRoute.clear();
					//bestRoute.add(start); // let us omit the last node
					min = totalCost;
				}
				return min;
			}
			if(k > 0)
			{
				//System.out.println("Dead-end");
				return Double.MAX_VALUE; // dead end
			}
		}
		else start.visit();
		
		for(WeightedTarget arrow : start.getNexts())
		{
			Node nextNode = arrow.target();
			
			if(!nextNode.isVisited())
			{
				if(!checkConsistency(nextNode, k))
				{
					//System.out.println("inconsistent!!!!");
					continue;
				}
				int weight = totalWeight + nextNode.getWeight();
				double cost = totalCost + cost(weight, arrow.weight());
				if(cost >= min) continue;
				double newMin = solver(nextNode, bestRoute, weight, cost, min, k+1);
				//System.out.println("Previous " + nextNode.getId() + " returns min " + newMin);
				if(newMin < min)
				{
					min = newMin;
					bestRoute.add(start);
					//System.out.println("Found new min " + min + " at " + start.getId() + " cost " + cost + " thanks to " + nextNode.getId());
				}
			}
		}
		start.unvisit();
		//System.out.println("Start " + start.getId() + " returns min " + min);
		return min;
	}

	private double cost(int weight, double distance) {
		return distance * (m_vMax - weight*(m_vMax - m_vMin)/m_kpSize);
	}


}
