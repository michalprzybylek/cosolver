/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package tskp;

import java.util.ArrayList;
import java.util.List;

public class Node {
	private int m_id;
	private int m_visited = 0;
	//private boolean m_mark = false;
	private int m_weight = 0;
	private List<WeightedTarget> m_nexts;
	
	
	public Node(int id, int weight, ArrayList<WeightedTarget> nexts)
	{
		m_id = id;
		m_weight = weight;
		m_nexts = nexts;
	}
	
	public void visit() {
		m_visited = 1; 
	}
	
	public void unvisit() {
		m_visited = 0;
	}
	
	public void mark() {
		m_visited += 2; 
	}
	
	public void unmark() {
		m_visited -= 2; 
	}
	
	public void untouch() {
		m_visited = 0;
	}
	
	public boolean isVisited() {
		return m_visited == 1;
	}
	
	public boolean isMarked() {
		return m_visited > 1;
	}
	
	public boolean isTouched() {
		return m_visited > 0;
	}

	public int getWeight() {
		return m_weight;
	}
	
	public int getId() {
		return m_id;
	}
	
	public List<WeightedTarget> getNexts()
	{
		return m_nexts;
	}

	public void addNext(WeightedTarget wt) {
		m_nexts.add(wt);
		
	}

	public void clearWeight() {
		m_weight = 0;
	}
	
}
