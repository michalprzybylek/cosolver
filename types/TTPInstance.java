/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package types;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

public class TTPInstance {
	
	private BasicGraph m_graph;
	private List<BasicItem> m_items = new ArrayList<BasicItem>();
	
	private BasicNode m_startNode;
	private int m_capacity;
	private double m_knapsackCost;
	private double m_minVelocity;
	private double m_maxVelocity;
	
	private JSONObject m_JSONinstance;
	
	public BasicGraph getGraph() {

		return m_graph;
	}
	
	public List<BasicItem> getItems(){
		return this.m_items;
	}

	public BasicNode getStartNode() {
		return m_startNode;
	}

	public int getCapacity(){
		return m_capacity;
	}
	
	public double getKnapsackCost() {
		return m_knapsackCost;
	}

	public double getMinVelocity() {
		return m_minVelocity;
	}

	public double getMaxVelocity() {
		return m_maxVelocity;
	}
	
	public BasicItem getItemById(int itemID){
		return this.getItems().get(itemID);
	}
	
	public JSONObject getJSONinstance() {
		return this.m_JSONinstance;
	}
	//constructor
	public TTPInstance(BasicGraph graph, List<BasicItem> items, int startNodeIndex, int capacity, double knapsackCost, double minVelocity, double maxVelocity, JSONObject instanceJSON) throws Exception{
		if(minVelocity > maxVelocity)
			throw new Exception("Min Velocity larger than Max Velocity");
		this.m_graph = graph;
		this.m_items = items;
		
		this.m_startNode = graph.getNodes().get(startNodeIndex);
		this.m_capacity = capacity;
		this.m_knapsackCost = knapsackCost;
		this.m_minVelocity = minVelocity;
		this.m_maxVelocity = maxVelocity;
		
		this.m_JSONinstance = instanceJSON;
	}
	
	public String toString(){
		return String.format("\n***Instance of***\nTravelling Thief Problem.\nStart-Node ID: %d. \n"
				+ "Capacity: %d Knapsack Cost: %f\nMin. Velocity: %f Max. Velocity: %f\n",
				this.getStartNode().getID(), this.getCapacity(), this.getKnapsackCost(), this.getMinVelocity(), this.getMaxVelocity())
				+ this.getGraph().toString()+"***INSTANCE END***";
	}
	
	//calculates cost of traveling given distance with given load
	//from prof. Michalewicz article
	public double getVelocity(int totalWeight){
		return this.m_maxVelocity - (this.m_maxVelocity-this.m_minVelocity)*((double)totalWeight/this.m_capacity);
	}
	
	public double getCost(int totalWeight, double distance){
		return this.m_knapsackCost * distance / this.getVelocity(totalWeight);
	}
}
