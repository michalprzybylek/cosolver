/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package types;

import java.util.ArrayList;
import java.util.List;

//graph representation
public class BasicGraph {
	private ArrayList<BasicNode> m_nodes = new ArrayList<>();
	
	public List<BasicNode> getNodes(){
		return m_nodes;
	}
	
	public void addNode(BasicNode node) {
		this.getNodes().add(node);
	}
	
	public BasicNode getNodeById(int id){
		return this.getNodes().get(id);
	}
	
	public void addEdge(int fromID, int toID, int length){
		this.getNodes().get(fromID).addEdge(this.getNodes().get(toID), length);
	}
	
	public int getMaxID(){
		return this.getNodes().size() -1;
	}
	
	public String toString(){
		String nodes = "";
		String edges = "";
		String items = "";
		for (BasicNode n : this.m_nodes) {
			nodes += n.toString()+"\n";
			edges += n.edgesToString() + "\n";
			items += "Items at Node: " + n.getID() + "\n" + n.itemsToString() + "\n";
		}
		
		return "[nodes]\n" + nodes + "\n[edges]\n" + edges + "[items]\n" + items;
	}
}
