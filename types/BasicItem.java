/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package types;

import java.util.ArrayList;
import java.util.List;

public class BasicItem{
	
	private List<BasicNode> m_nodes = new ArrayList<BasicNode>();
	private int m_id;
	private int m_weight;
	private int m_value;
	
	public List<BasicNode> getNodes(){
		return m_nodes;
	}
	
	public int getID(){
		return this.m_id;
	}
	
	public int getWeight() {
		return m_weight;
	}

	public int getValue() {
		return m_value;
	}
	
	public double getValueOverWeight(){
		return ((double)this.m_value)/  this.m_weight;
	}
	
	public void addNode(BasicNode node){
		getNodes().add(node);
	}
	
	public boolean isAvailableAtNode(BasicNode node){
		for(BasicNode n : this.getNodes())
			if (n == node)
				return true;
		return false;
	}
	
	public BasicItem(int id, int weight, int value){
		this.m_id = id;
		this.m_weight = weight;
		this.m_value = value;
	}
	
	public String toString(){
		return "value: " +  this.getValue() + ", weight: " + this.getWeight() 
				+ ", v/w: " + this.getValueOverWeight();
	}


}
