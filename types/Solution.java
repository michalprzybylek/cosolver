/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



package types;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Solution {
	
	List<Integer> nodeSequence; //node sequence by id
	Map<Integer,List<Integer>> selectedItems;
	TTPInstance instance;
	
	private int totalValue = 0;
	private int totalWeight = 0;
	private double travellingTime = 0;
	private double result;
	
	public int getTotalValue(){
		return totalValue;
	}
	
	public double travellingTime(){
		return travellingTime;
	}
	
	public Solution(List<Integer> nodeSequence, Map<Integer,List<Integer>> selectedItems, TTPInstance instance) throws Exception{
		this.nodeSequence = nodeSequence;
		this.selectedItems = selectedItems;
		this.instance = instance;
		if(nodeSequence.size() != instance.getGraph().getNodes().size())
			throw new Exception ( String.format("Some nodes missed. delivered %d --- original: %d", nodeSequence.size(), instance.getGraph().getNodes().size() ));
		
		this.nodeSequence.add(nodeSequence.get(0));
		
		calculateResult();
	}
	
	
	
	
	private void calculateResult() throws Exception{
		
		BasicNode startNode = instance.getStartNode();
		
		if(startNode.getID() != this.nodeSequence.get(0))
			throw new Exception("Wrong start node.");
		
		Set<Integer> visited = new HashSet<Integer>();
	
		for(int i = 0; i < this.nodeSequence.size()-1 ;i++){
			BasicNode current = instance.getGraph().getNodeById( this.nodeSequence.get(i) );
			if(visited.contains(i))
			{
				throw new Exception("Node specified multiple times.");
			}
			visited.add(i);
			for(int itemID : selectedItems.get(current.getID())){
				BasicItem item = this.instance.getItemById(itemID);
				if(!item.isAvailableAtNode(current))
					throw new Exception(String.format("item: %d not available at node: %d", item.getID(), current.getID()));
				totalValue += item.getValue();
				totalWeight += item.getWeight();
			}
			if(totalWeight > instance.getCapacity())
				throw new Exception("Capacity " +  instance.getCapacity() + " limit exceeded " + totalWeight);
			
			BasicNode next = instance.getGraph().getNodeById( this.nodeSequence.get(i+1) );
			
			if(!current.getEdges().keySet().contains(next))
				throw new Exception(String.format("no edge from: %d to %d", current.getID(), next.getID()));

			travellingTime += current.getEdges().get(next) / instance.getVelocity(totalWeight);	
		}
		this.result = totalValue - (travellingTime * instance.getKnapsackCost());
	}
	
	public double getResult(){
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	private String getJSON(){
		JSONObject obj = new JSONObject();
		
		JSONArray path = new JSONArray();
		for(Integer i : this.nodeSequence) {
			JSONObject node = new JSONObject();
			node.put("id", i);
			JSONArray items = new JSONArray();
			for(Integer j : this.selectedItems.get(i)){
				items.add(j);
			}
			node.put("selectedItems", items);
			path.add(node);
		}
		
		obj.put("path", path);
		obj.put("result", this.getResult());
		
		JSONObject instance = this.instance.getJSONinstance();
		instance.put("solution", obj);
		
		return instance.toString();
	}
	
	public void saveToFile(String path){
		
		try{
			File file = new File(path);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(getJSON());
			bw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
	}
}
