/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package converter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import types.BasicGraph;
import types.BasicItem;
import types.BasicNode;
import types.TTPInstance;

public class FastJSONLoader {

	//main level
		static final String NODES_TAG = "nodes";
		static final String EDGES_TAG = "edges";
		static final String ITEMS_TAG = "items";
		static final String NODE_ITEM_PAIRS_TAG = "itemsAtNodes";
		
		static final String START_NODE_TAG = "startNode";
		
		static final String GENERAL_INFO_TAG = "generalInfo";
		
		static final String CAPACITY_TAG = "capacity";
		static final String KNAPSACK_COST_TAG = "knapsackCost";
		static final String MIN_VELOCITY_TAG= "minVelocity";
		static final String MAX_VELOCITY_TAG= "maxVelocity";
		
		static final String ID_TAG = "id";
		
		
		//item
		static final String ITEM_WEIGHT_TAG = "weight";
		static final String ITEM_VALUE_TAG = "value";
		//edge
		static final String EDGE_ORIGIN_TAG = "from";
		static final String EDGE_DESTINATION_TAG = "to";
		static final String EDGE_LENGTH_TAG = "length";
		//NodeItem
		static final String ITEM_TAG = "item";
		static final String NODE_TAG = "node";
		
	
	/*public static void main(String[] args) throws IOException, Exception {
		
		String fileName = "instanceRand";
		String path = new java.io.File("").getAbsolutePath() + String.format("\\instances\\%s.json", fileName) ;
		TTPInstance instance = FastJSONLoader.load(path);
		System.out.println(instance.toString());
		ExactSolver es = new ExactSolver(instance);
		Solution solution = es.solve();
		solution.saveToFile(new java.io.File("").getAbsolutePath() + String.format("\\instances\\%s_Solution.json", fileName)) ;
	}*/
	
	//path --- absolute path to .json file with the instance
		public static TTPInstance load(String path) 
				throws IOException, Exception {
			
			String content = new String(Files.readAllBytes(Paths.get(path))); 	
			JSONObject instance = (JSONObject) JSONValue.parse(content);
			
			if(instance == null)
				throw new Exception("File not in JSON format.");
						
			JSONArray nodesJSON = (JSONArray) instance.get(FastJSONLoader.NODES_TAG);
			JSONArray edgesJSON = (JSONArray) instance.get(FastJSONLoader.EDGES_TAG);
			JSONArray itemsJSON = (JSONArray) instance.get(FastJSONLoader.ITEMS_TAG);
			JSONArray nodeItemJSON = (JSONArray) instance.get(FastJSONLoader.NODE_ITEM_PAIRS_TAG);
			
			BasicGraph graph = new BasicGraph();
			ArrayList<BasicItem> items = new ArrayList<BasicItem>();
			
			
			for (Object node : nodesJSON){
				graph.addNode(new BasicNode(FastJSONLoader.getIntegerValue((JSONObject) node, FastJSONLoader.ID_TAG)));
			}
			
			for (Object item : itemsJSON){
				items.add(new BasicItem(
						FastJSONLoader.getIntegerValue((JSONObject) item, FastJSONLoader.ID_TAG),
						FastJSONLoader.getIntegerValue((JSONObject) item, FastJSONLoader.ITEM_WEIGHT_TAG),
						FastJSONLoader.getIntegerValue((JSONObject) item, FastJSONLoader.ITEM_VALUE_TAG)
				));
			}
			
			for (Object edge : edgesJSON){
				int fromId = FastJSONLoader.getIntegerValue((JSONObject) edge, FastJSONLoader.EDGE_ORIGIN_TAG);
				int toId = FastJSONLoader.getIntegerValue((JSONObject) edge, FastJSONLoader.EDGE_DESTINATION_TAG);
				double length = FastJSONLoader.getPositiveDoubleValue((JSONObject) edge, FastJSONLoader.EDGE_LENGTH_TAG);
				graph.getNodeById(fromId).addEdge(graph.getNodeById(toId), length);
			}
			
			for (Object nodeItem : nodeItemJSON) {
				int nodeId = FastJSONLoader.getIntegerValue((JSONObject) nodeItem, FastJSONLoader.NODE_TAG);
				int itemId = FastJSONLoader.getIntegerValue((JSONObject) nodeItem, FastJSONLoader.ITEM_TAG);
				graph.getNodeById(nodeId).addItem(items.get(itemId));
			}
			
			JSONObject generalInfoJSON = (JSONObject) instance.get(FastJSONLoader.GENERAL_INFO_TAG);
			
			return new TTPInstance(graph, items, 
					FastJSONLoader.getIntegerValue(instance, FastJSONLoader.START_NODE_TAG),
					FastJSONLoader.getIntegerValue(generalInfoJSON, FastJSONLoader.CAPACITY_TAG),
					FastJSONLoader.getPositiveDoubleValue(generalInfoJSON, FastJSONLoader.KNAPSACK_COST_TAG),
					FastJSONLoader.getPositiveDoubleValue(generalInfoJSON, FastJSONLoader.MIN_VELOCITY_TAG),
					FastJSONLoader.getPositiveDoubleValue(generalInfoJSON, FastJSONLoader.MAX_VELOCITY_TAG),
					instance
					);
		}
		
		private static double getPositiveDoubleValue(JSONObject JSONobject, String field) throws Exception {
			
			Object number = JSONobject.get(field);
			
			
			if(number instanceof java.lang.Long){
				return (double)((long)number);
			}
			else if(number instanceof java.lang.Double)
				return (double) number;
	
			throw new Exception(String.format("Wrong number: %s in %s ", field, JSONobject.toJSONString()));
		}
			
		private static int getIntegerValue(JSONObject JSONobject, String field) throws Exception {
			
			Object number = JSONobject.get(field);
			
			
			if(number instanceof java.lang.Long){
				return ((Long)number).intValue();
			}
			
			throw new Exception(String.format("Wrong number: %s in %s ", field, JSONobject.toJSONString()));
		
		}

	
}
