/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



package solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import types.BasicItem;
import types.BasicNode;
import types.Solution;
import types.TTPInstance;
import converter.FastJSONLoader;


public class ExactSolver {
	
	TTPInstance ttpInstance;
	
	Solution currentBestSolution = null;

	ESNode start;
	List<ESNode> nodes = new ArrayList<ESNode>();
	List<ESNode> path = new ArrayList<ESNode>();
	List<ESNode> activeNodes = new ArrayList<ESNode>();
	
	List<ESItem> items = new ArrayList<ESItem>();
	int minimumWeight;
	
	int numberOfPaths = 0;
	
	public static void main(String[] args) throws IOException, Exception{
		
		if(args.length != 2) {
			System.err.print(args[0] + " " + args[1]);
			System.err.println("args: file_with_instance output_file_with_solution");
			System.exit(1);
		}
		TTPInstance instance = FastJSONLoader.load(args[0]);
		ExactSolver es = new ExactSolver(instance);
		es.solve().saveToFile(args[1]);;
		
	}
	
	public ExactSolver(TTPInstance ttpInst) throws Exception{
		
		this.ttpInstance = ttpInst;
		
		this.setNodesAndItems();
		this.activeNodes.addAll(nodes);
		
		this.start = this.nodes.get( ttpInstance.getStartNode().getID() );
		this.path.add(start);
		this.path.add(start);
		start.distanceToEnd = 0.0;
		activeNodes.remove(start);
		start.resetToBeChecked(activeNodes);
		
	}
	
	public Solution solve() throws Exception {
		
		while( this.path.size() > 1 ){
			while( this.getFirstOnPath().areThereMoreEdgesToBeChecked() ){
				if( !this.isCycleTheoreticallyFeasible( ) ) {
					this.removeFirstActive();
				}
				
				//to-do: check if best feasible solution can beat current best
				
				this.checkNextEdge();
			}
			
			if(this.path.size() == this.nodes.size()+1 && this.getFirstOnPath().edgesIn.containsKey(this.start)){
				
				start.setDistanceToEndAndMinimalCost(this.getFirstOnPath());
				
				this.printPath();
				this.solveKnapsack();
				removeFirstActive();
				removeFirstActive();
				
				start.distanceToEnd = 0;
			}
		
			
			while (this.path.size() > 1 && !this.getFirstOnPath().areThereMoreEdgesToBeChecked() ){
				this.removeFirstActive();
			}
		}
		
		return currentBestSolution;
	}
	
	private ESNode getFirstOnPath(){
		return this.path.get(1);
	}
	
	
	//checks whether there is at least one edge among active nodes pointing at start node
	private boolean isCycleTheoreticallyFeasible(){
		for(ESNode active : this.activeNodes){
			if(active.originalNode.getEdges().containsKey(start.originalNode))
				return true;
		}
		return false;
	}
	
	private void addNodeToPath(ESNode node) throws Exception{
		if(path.contains(node))
			throw new Exception(String.format("%d already in path", node.originalNode.getID()) );
		
		node.setDistanceToEndAndMinimalCost(this.getFirstOnPath());
		
		
		path.add(1, node);
		activeNodes.remove(node);
		
		node.resetToBeChecked(activeNodes);
		
	}
	
	private void removeFirstActive(){
		ESNode end = this.getFirstOnPath();
		path.remove(end);
		activeNodes.add(end);
	}
	
	private void checkNextEdge() throws Exception{
		ESNode next = this.getFirstOnPath().getNextToBeChecked();
		if(next != null)
			this.addNodeToPath(next);
	}
	
	
	private Solution createSolution() throws Exception{			
		
		List<Integer> sequence = new ArrayList<Integer>();
		Map<Integer, List<Integer>> selectedItems = new HashMap<Integer, List<Integer>>();
		
		for(int i = 0; i < this.path.size()-1; i++){
			ESNode node = this.path.get(i);
			sequence.add(node.originalNode.getID());
			List<Integer> items = new ArrayList<Integer>();
			for(ESItem item : node.selectedItems){
				items.add(item.originalItem.getID());
			}
			selectedItems.put(node.originalNode.getID() , items);
		}
		
		return new Solution(sequence, selectedItems, this.ttpInstance);
	}
	
	private void solveKnapsack() throws Exception{
				
		for(ESNode node : this.nodes){
			node.setItemsMinimalCost();
			node.clearSelectedItems();
		}
		
		if(this.currentBestSolution == null || this.currentBestSolution.getResult() < -this.start.minimalCost){
			this.currentBestSolution = createSolution();
			System.out.println( String.format("Solution updated. Solution result: %f start.minimal: %f", this.currentBestSolution.getResult(), -start.minimalCost));
		}
		
		this.sortItemsByMaximumValueOverWeight();
		
		double totalMaximumSelectedValue = 0.0;
		int capacityLeft = this.ttpInstance.getCapacity();
		
		boolean [] selected = new boolean[this.items.size()];
		int position = 0;
		selected[position] = true;
		boolean goingRight = true;
		
		while( position >= 0 ) {
			//System.out.println(capacityLeft);
			if( goingRight ){
				while( position < selected.length ){
					
					if(capacityLeft < this.minimumWeight)
						break;
					
					ESItem item = items.get(position);
					if(item.getWeight()<= capacityLeft){
						capacityLeft -= item.getWeight();
						totalMaximumSelectedValue += item.getMaximumValue();
						item.select();
						selected[position] = true;
						
						if(totalMaximumSelectedValue - this.start.minimalCost > currentBestSolution.getResult()){
							Solution candidateSolution = createSolution();
							if(candidateSolution.getResult() > currentBestSolution.getResult()){
								System.out.println(String.format("Better Solution!! OLD: %f   NEW: %f", this.currentBestSolution.getResult(), candidateSolution.getResult()));
								currentBestSolution = candidateSolution;
							}
						}
					}
					position++;
					
				}
				goingRight = false;
			}
			else{
				if(position == selected.length) position--;
				while( position >= 0 && !selected[position]){
					position--;
				}
				
				//printArray( selected );
				
				if(position >= 0) {
					ESItem item = items.get(position);
					capacityLeft += item.getWeight();
					totalMaximumSelectedValue -= item.getMaximumValue();
					item.unselect();
					selected[position++] = false;
				}
				goingRight = true;
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void printArray( boolean [] array ){ 
		
		String result = "";
		for(boolean b  : array)
			if(b)
				result+='1';
			else result+='0';
		System.out.println(result);
	}
	
	private void sortItemsByMaximumValueOverWeight(){
		Collections.sort(items, new Comparator<ESItem>(){

			public int compare(ESItem arg0, ESItem arg1) {
				if(arg0.getMaximumValueOverWeight()>arg1.getMaximumValueOverWeight())
					return -1;
				else if (arg0.getMaximumValueOverWeight()<arg1.getMaximumValueOverWeight())
					return 1;
				else 
					return 0;
			}
			
		});
	}
		
	private void setNodesAndItems(){
		//adding nodes
		for (BasicNode n : ttpInstance.getGraph().getNodes()){
			ESNode node = new ESNode(n, this);
			this.nodes.add(node);
			for (BasicItem it : n.getItems()){
				ESItem item = new ESItem(node, it);
				this.items.add(item);
				node.items.add(item);
			}
		}
		//establishing edges
		for (ESNode node : this.nodes){
			for(BasicNode org : node.originalNode.getEdges().keySet()){
				ESNode edgeEndPoint = this.nodes.get(org.getID()); 
				edgeEndPoint.edgesIn.put(node, node.originalNode.getEdges().get(org));
			}
		}
		//setting minumWeight
		minimumWeight = Integer.MAX_VALUE;
		for (BasicItem item : this.ttpInstance.getItems()){
			if(item.getWeight() < minimumWeight)
				minimumWeight = item.getWeight();
		}
	}
	
	public void printPath(){
		String result = this.path.get(0).originalNode.getID() + "";
		double distance = 0.0;
		for(int i = 1; i < path.size(); i++){
			distance +=  path.get(i).edgesIn.get(path.get(i-1));
			result += String.format(" --(%f)--> %d", path.get(i-1).distanceToEnd, path.get(i).originalNode.getID());
		}
		System.out.println(++this.numberOfPaths + ": " + result + " (" + distance + ")");
	}
		
	//Extension of Node for purpose of Exact Solution
	private class ESNode {
		
		BasicNode originalNode;
		ExactSolver solver;
		
		List<ESItem> items = new ArrayList<ESItem>();
		Set<ESItem> selectedItems = new HashSet<ESItem>();
		
		Map<ESNode, Double> edgesIn = new HashMap<ESNode, Double>();
		Set<ESNode> edgesToBeChecked = new HashSet<ESNode>(); //changes with each cycle
		double shortestToBeChecked;
		
		double distanceToEnd = Double.POSITIVE_INFINITY;
		double minimalCost = Double.POSITIVE_INFINITY;
		
		public ESNode(BasicNode original, ExactSolver solver){
			this.originalNode = original;
			this.solver = solver;
			
		}
		
		public void setDistanceToEndAndMinimalCost(ESNode next){
			this.distanceToEnd = next.distanceToEnd + next.edgesIn.get(this);
			this.minimalCost = solver.ttpInstance.getCost(0, this.distanceToEnd);
		}
		
		public void resetToBeChecked( Collection<ESNode> activeNodes ){
			this.edgesToBeChecked.clear();
			for(ESNode edgeIn : this.edgesIn.keySet()){
				if(activeNodes.contains(edgeIn))
					this.edgesToBeChecked.add(edgeIn);
			}
			this.resetShortest();
		}
		public ESNode getNextToBeChecked(){
			Iterator<ESNode> iter = this.edgesToBeChecked.iterator();
			if(iter.hasNext())
			{
				ESNode edge = iter.next();
				this.edgesToBeChecked.remove(edge);
				if(edgesIn.get(edge) == this.shortestToBeChecked){ 
					resetShortest();
				}
				return edge;
			}
			else
				return null;	
		}
		
		public boolean areThereMoreEdgesToBeChecked(){
			return this.edgesToBeChecked.size() > 0;
		}
		
		private void resetShortest(){
			Iterator<ESNode> iter = edgesToBeChecked.iterator();
			this.shortestToBeChecked = Double.MAX_VALUE;
			while(iter.hasNext()){
			ESNode next = iter.next();
			if(edgesIn.get(next) < this.shortestToBeChecked)
				this.shortestToBeChecked = edgesIn.get(next);
			}
		}
		
		public void setItemsMinimalCost(){
			for(ESItem item : this.items){
				item.setMinimalCostOfTranport ( this.solver.ttpInstance.getCost(item.getWeight(), this.distanceToEnd) - this.minimalCost);
			}
		}
		
		public void clearSelectedItems(){			
			this.selectedItems.clear();
		}
	}
	
	private class ESItem {
		BasicItem originalItem;
		ESNode location;
		
		double minimalCostOfTranport;
		
		public ESItem(ESNode location, BasicItem item){
			this.location = location;
			this.originalItem = item;
		}
		
		public void setMinimalCostOfTranport(double cost) {
			this.minimalCostOfTranport = cost;
		}

		public int getWeight(){
			return this.originalItem.getWeight();
		}
		
		public double getValue(){
			return this.originalItem.getValue();
		}
		
		public double getMaximumValue(){
			return this.getValue() - this.minimalCostOfTranport;
		}
		
		public double getMaximumValueOverWeight(){
			return this.getMaximumValue() / this.getWeight();
		}
		
		public void select(){
			this.location.selectedItems.add(this);
		}
		
		public void unselect(){
			this.location.selectedItems.remove(this);
		}	
	}
}



