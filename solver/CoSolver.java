/**
 * Copyright (c) 2014, Artur Grzesiak and Michal R. Przybylek
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Publications in scientific journals/conference proceedings using either
 *    directly or indirectly results obtained by this software must cite article:
 *    "Socially inspired algorithms for the travelling thief problem" by Mohammad
 *    R. Bonyadi, Zbigniew Michalewicz, Michal R. Przybylek, and Adam Wierzbicki,
 *    In Proceedings of the 2014 conference on Genetic and evolutionary computation
 *    (GECCO '14). ACM, New York, NY, USA, 421-428. DOI=10.1145/2576768.2598367
 *    http://doi.acm.org/10.1145/2576768.2598367 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package solver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import kpr.Item;
import kpr.KP;

import tskp.Node;
import tskp.TSKP;
import tskp.WeightedTarget;
import types.BasicItem;
import types.BasicNode;
import types.Solution;
import types.TTPInstance;
import utils.Pair;

import converter.FastJSONLoader;

public class CoSolver {
	private double m_theta = 1.0;
	private TTPInstance m_instance;

	public static void main(String[] args) throws IOException, Exception {

		if (args.length != 2) {
			System.err.print(args[0] + " " + args[1]);
			System.err
					.println("args: file_with_instance output_file_with_solution");
			System.exit(1);
		}
		CoSolver es = new CoSolver(FastJSONLoader.load(args[0]));
		es.solve().saveToFile(args[1]);

	}

	CoSolver(TTPInstance instance) {
		m_instance = instance;
	}

	TSKP buildTSKP(Set<Integer> items) {
		Node start = buildGraph(m_instance.getStartNode(), items,
				new HashMap<BasicNode, Node>());
		start.clearWeight();
		TSKP tskp = new TSKP(start, m_instance.getCapacity(),
				m_instance.getMinVelocity(), m_instance.getMaxVelocity());
		return tskp;
	}

	Node buildGraph(BasicNode start, Set<Integer> items,
			Map<BasicNode, Node> visited) {
		Node node = visited.get(start);
		if (node == null) {
			int weight = 0;
			for (BasicItem item : start.getItems()) {
				if (items.contains(item.getID()))
					weight += item.getWeight();
			}
			node = new Node(start.getID(), weight,
					new ArrayList<WeightedTarget>(start.getEdges().size()));
			//System.out.println("Created node " + node.getId());
			visited.put(start, node);

			for (Entry<BasicNode, Double> entry : start.getEdges().entrySet()) {
				Node next = buildGraph(entry.getKey(), items, visited);
				WeightedTarget wt = new WeightedTarget(entry.getValue(), next);
				node.addNext(wt);
			}
			
			Collections.sort(node.getNexts());
		}
		return node;
	}

	KP buildKP(List<BasicNode> nodes, Map<Integer, Pair<Integer, Double> > penalties) {
		KP kp = new KP(m_instance.getCapacity());

		for (int i = 0; i < nodes.size(); i++) {
			BasicNode node = nodes.get(i);
			for (BasicItem item : node.getItems()) {
				Pair<Integer, Double> pair = penalties.get(i);
				if(pair == null)
				{
					pair = new Pair<Integer, Double>(0, 0.0);
					penalties.put(i, pair);
					
				}
				int totalWeight = pair.fst();
				Double distance = pair.snd();
				int value = item.getValue();
				int weight = item.getWeight();
				int recalculatedValue = estimateRealValue(value, weight, totalWeight, distance);
				//System.out.println("+++ distance " + distance + " value " + value + " recalculated value " + recalculatedValue + " node " + node.getID());
				Item newItem = new Item(item.getID(), node.getID(), item.getWeight(), recalculatedValue);
				
				kp.add(newItem);
			}
		}
		return kp;
	}

	public Solution solve() throws Exception {
		//int maxWeight = findMaxWeight();
		//double theta = 1;
		int maxIter = 32;
		List<BasicNode> nodes = m_instance.getGraph().getNodes();
		Map<Integer, Pair<Integer, Double> > penalties = new HashMap<Integer, Pair<Integer, Double> >();
		// List<Double> weights = Collections.nCopies(nodes.size(), 0.0);
		List<Item> items = new ArrayList<Item>();
		List<Node> cycle = new ArrayList<Node>(nodes.size());

		buildTSKP(getIds(items)).solve(cycle);
		recalculatePenaltiesInit(cycle, penalties);
		buildKP(nodes, penalties).solve(items);
		//penalties.clear();
		
		Solution bestSolution = buildSolution(cycle, items); 
		double bestValue = bestSolution.getResult();

		System.out.println("INITIAL VALUE " + bestValue);
		
		for(int k = 0 ; k < maxIter ; k++) {
			KP kp = buildKP(nodes, penalties);

			kp.solve(items);
			
			/*
			System.out.println("Max weight " + kp.getMaxWeight()
					+ " found solution wieght " + kp.getSolutionWeight());

			for(Item item : items)
			{
				System.out.println("****Item " + item.getId() + " value " + item.getValue() + " weight " + item.getWeight());

			}
			*/
			
			TSKP tskp = buildTSKP(getIds(items));
			tskp.solve(cycle);

			Solution solution = buildSolution(cycle, items);

			double value = solution.getResult();

			System.out.println("VALUE " + value);
			
			if (value <= bestValue) { // poor solution...
				m_theta = m_theta / 2;
				continue;
			}
			
			k--;

			bestValue = value;
			bestSolution = solution;

			//List<Double> distances = calculateDistances(cycle);
			recalculatePenalties(cycle, penalties);
		}

		/*
		 * for(int i = nodes.size() - 1 ; i >= 0 ; i--) {
		 * 
		 * }
		 */
		// calculate real profit
		// calculate penalties (times first)

		//System.out.println("Number of iterations " + k);

		
		return bestSolution;
	}

	private int findMaxWeight() {
		int acc = 0;
		for (BasicItem item : m_instance.getItems()) {
			acc = Math.max(acc, item.getWeight());
		}
		return acc;
	}

	private Solution buildSolution(List<Node> cycle, List<Item> items)
			throws Exception {
		List<Integer> nodeSequence = new ArrayList<Integer>(cycle.size() + 1);
		Map<Integer, List<Integer>> selectedItems = new HashMap<Integer, List<Integer>>();

		for (Node node : cycle) {
			Integer nodeId = node.getId();
			// System.out.println("Cycle node " + nodeId.toString());
			nodeSequence.add(nodeId);
			selectedItems.put(nodeId, new ArrayList<Integer>());
		}

		Integer calculatedWeight = 0;

		for (Item item : items) {
			calculatedWeight += item.getWeight();
			Integer itemId = item.getId();
			Integer nodeId = item.getNodeId();

			if (item.getInKnapsack() == 0) {
				System.err.println("Item " + itemId + " not in knapsack.");
				System.exit(0);
			}

			List<Integer> itemList = selectedItems.get(nodeId);
			if (itemList == null) {
				System.err.println("Missing node " + nodeId.toString());
				System.exit(0);
			}
			itemList.add(itemId);
		}

		//System.out.println("Calculated weight " + calculatedWeight);

		// Solution gets cycle without last node

		return new Solution(nodeSequence, selectedItems, this.m_instance);
	}

	private Set<Integer> getIds(List<Item> items) {
		Set<Integer> result = new HashSet<Integer>();
		for (Item item : items) {
			result.add(item.getId());
		}
		return result;
	}

	private List<Double> calculateDistances(List<Node> cycle) {
		List<Node> extendedCycle = new ArrayList<Node>(cycle);
		extendedCycle.add(cycle.get(0));
		List<Double> distances = new ArrayList<Double>(cycle.size());

		double acc = 0.0;

		for (int i = extendedCycle.size() - 2; i > 0; i--) {
			Node node = extendedCycle.get(i);
			Node nextInCycle = extendedCycle.get(i + 1);
			for (WeightedTarget next : node.getNexts()) {
				if (next.target() == nextInCycle) {
					acc += next.weight();
					distances.add(acc);
					break;
				}
			}
		}
		distances.add(0.0);
		Collections.reverse(distances);

		if (distances.size() != cycle.size()) {
			System.out.println("size(distance) != size(cycle)");
			System.exit(0);
		}

		return distances;
	}

	private List<Double> calculateWeights(List<Node> cycle) {
		List<Double> weights = new ArrayList<Double>(cycle.size());

		double acc = 0.0;

		weights.add(acc);
		for (int i = 1; i < cycle.size(); i++) {
			acc += cycle.get(i).getWeight();
			weights.add(acc);
		}

		return weights;
	}
	
	private void recalculatePenalties(List<Node> cycle, Map<Integer, Pair<Integer, Double> > penalties) {
		List<Double> distances = calculateDistances(cycle);
		for (int i = 1; i < distances.size(); i++) {
			double distance = distances.get(i);
			int nodeId = cycle.get(i).getId();
			int prevWeight = cycle.get(i-1).getWeight();
			//System.out.println("Distance " + distance + " prevWeight " + prevWeight + " at " + i + " node " + nodeId);
			penalties.put(nodeId, new Pair<Integer, Double>(prevWeight, distance));
		}
	}
	
	private void recalculatePenaltiesInit(List<Node> cycle, Map<Integer, Pair<Integer, Double> > penalties) {
		List<Double> distances = calculateDistances(cycle);
		for (int i = 1; i < distances.size(); i++) {
			double distance = distances.get(i);
			int nodeId = cycle.get(i).getId();
			int prevWeight = 0;
			//System.out.println("Distance " + distance + " prevWeight " + prevWeight + " at " + i + " node " + nodeId);
			penalties.put(nodeId, new Pair<Integer, Double>(prevWeight, distance));
		}
	}
	
	
	private int estimateRealValue(int value, int weight, int prevTotalWeight, double distance)
	{
		int totalWeight = prevTotalWeight + weight;
		if(totalWeight > m_instance.getCapacity()) return 0;
		double prevCost = m_instance.getCost(prevTotalWeight, distance);
		double cost = m_instance.getCost(totalWeight, distance);
		double delta = cost - prevCost;
		return Math.max(0, (int)Math.round(value - m_theta*delta));
	}
	/*
	 * private Set<Integer> calculateRealProfit(List<Item> items) { Set<Integer>
	 * result = new HashSet<Integer>(); for(Item item : items) {
	 * result.add(item.getId()); } return null; }
	 */

}
